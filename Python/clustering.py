from sshtunnel import SSHTunnelForwarder
import collections
import mysql.connector
import tmconnect
from mysql.connector import Error
import numpy as np
import math
# ------------
# Installation
# ------------
# tmconnect.py muss importiert werden. Des Weiteren ist eine Installation von numpy und mysql.connector notwendig

# ----------------------------------------------------
# Funktionen für Datenbankzugriff und Vektorerstellung
# ----------------------------------------------------
db = ""
def go(_connection):
    global db
    db = _connection

def getSentenceData(s): # Extrahiert Informationen wie z.B. POS-Tags für einen Satz für weitere Bearbeitung
    out = collections.OrderedDict()
        #Hole den Satz mit s_id = s
    sen_cur = db.cursor()
    sen_cur.execute('''SELECT sentence
                            FROM sentences
                            WHERE s_id = {}'''.format(s))
        
    sen_ = str(sen_cur.fetchone()[0])
    print("Bearbeite Satz:\n'{}'".format(sen_))
    out['sentence'] = sen_
        
        #Nimm t_id (id), word (form), lemma, upos, xpos, feats aus dep_types für jedes Wort aus dem Satz
    dty_cur = db.cursor()
    dty_cur.execute('''SELECT word, feats
                            FROM dep_types as dty
                            LEFT JOIN dep_tokens as dto
                            ON dty.t_id = dto.t_id
                            WHERE dto.s_id = {}'''.format(s))
    resp = dty_cur.fetchall()
    for r in resp:
        out[r[0]]= r[1]
    sen_cur.close()
    dty_cur.close()
    return out
        #db.close()
        #print("Datenbank geschlossen")

# Extrahiert bestimmte Anzahl (limit) an Sätzen für ein verb
def getSentencesByVerb(verb, limit):
    out = []
    cur = db.cursor()
    cur.execute('''SELECT dto.s_id
                        FROM dep_tokens as dto
                        LEFT JOIN dep_types as dty
                        ON dty.t_id = dto.t_id
                        WHERE dty.lemma like "{}"
                        LIMIT {}'''.format(verb.lower(), limit))
    resp = cur.fetchall()
    for r in resp:
        out.append(r[0])
    cur.close()
    return out
        
        #db.close()
        #print("Datenbank geschlossen")


def l2Norm(a): # L2-Norm, gibt einen festen Wert und keinen Vektor zurück
    return np.linalg.norm(a)


def norm(a): # Normiere Vektor, s.d. Betrag(Vektor) = 1
    result = 1 / np.linalg.norm(a)
    return a * result

# Kosinus-Ähnlichkeit berechnen zwischen zwei Vektoren a und b
def cosSim(a, b):
    upper = np.dot(a, b)
    lower = l2Norm(a) * l2Norm(b)
    # 1 means extremely similiar, 0 means orthogonal, -1 means extremely different
    result = upper / lower
    if(result > 1.0):
        result = 1.0
    return result

# übergebe Liste 'list' mit allen Vektoren, damit zwischen allen Vektoren die Distanzen ausgerechnet werden können. Wird für das Clustering nicht mehr benötigt
def createDistanceMatrix(list):
    countVectors = len(list) # Anzahl Vektoren
    #erstelle 2D-Array bzw. Matrix, in der alle Distanzen zueinander gespeichert werden
    matrix = [[0 for x in range(countVectors)] for y in range(countVectors)]
    index1 = 0
    index2 = 0
    # Vergleiche jedes Element mit jedem Element danach. Verbesserung von O(n^2) zu O(n log n)
    # alle Distanzen zwischen den Vektoren sind nun in matrix gespeichert, z.B. Distanz zwischen 1. Vektor x und 2. Vektor y wäre matrix[0][1] = 3, wenn matrix so aussieht:
    #   x y
    # x 0 3
    # y 0 0
    # x zu x irrelevant, daher 0. y zu x auch 0, da wir schon x zu y haben. Spart Zeit- und Speicheraufwand 
    # Distanz zwischen x und y wäre 3 in diesem Beispiel: cosSim(x, y) = 3
    for vector1 in list:
        index2 = index1 + 1
        for vector2 in list[index2:]:
            result = cosSim(vector1, vector2)
            matrix[index1][index2] = result
            index2 += 1
        index1 += 1
        index2 = index1
    return matrix

# ------------------------------
# Eingabe der gewünschten Verben
# ------------------------------
def verbInput(): # fragt nach n und liest n Verben vom Nutzer ein und gibt eine Liste aller Verben zurück
    print("Für wieviele Verben soll ein Satzbauplanvektor erstellt werden? Bitte natürliche Zahl eingeben:")
    n = input()
    n = int(n)
    verben = []
    for i in range(1, n + 1):
        print("Bitte " + str(i) + ". Verb eingeben:")
        s = input()
        verben.append(s)
    return verben


#-----------------
# Vektorerstellung
# ----------------
def getIDs(verbs): # holt n ID´s bzw. Sätze für jedes Verb. Hohe n erhöhen die Laufzeit erheblich bei vielen Verben!
    # gibt ein Mapping zurück, wobei jedes verb eine Liste von ID´s zurückgibt
    print("Wieviele Sätze pro Verb sollen extrahiert werden? Bitte natürliche Zahl eingeben:")
    n = input()
    n = int(n)
    allIDs = {}
    for verb in verbs:
        ids = getSentencesByVerb(verb, n + 1)
        allIDs.update({verb : ids})
    return allIDs

# findSBP findet den SBP des Satzes heraus und gibt in zurück
# subj = Subjekt, dobj = Akkusativ, iobj = Dativ, 
def findSBP(nominativ, genitiv, akkusativ, dativ):
    if nominativ:
        if genitiv:
            if akkusativ:
                if dativ:
                    return "Ksub,Kgen,Kakk,Kdat"
                else:
                    return "Ksub,Kgen,Kakk"
            else:
                if dativ:
                    return "Ksub,Kgen,Kdat"
                else:
                    return "Ksub,Kgen"
        else:
            if akkusativ:
                if dativ:
                    return "Ksub,Kakk,Kdat"
                else:
                    return "Ksub,Kakk"
            else:
                if dativ:
                    return "Ksub,Kdat"
                else:
                    return "Ksub"
    else:
        return "null"

# zählt, wie oft ein Verb in entsprechenden SBP´s vorkommt. Gibt dictionary zurück
def countFrequency(satzIDs):
    #speichere alle SBP´s und die Anzahl, wie oft diese vorkommen
    dic = {}
    dic["null"] = 0 # null, falls kein anderer Satzbauplan für dieses Verb gefunden wird. Hoher null-Wert sagt etwas über die Ungewöhnlichkeit des Verbs aus
    dic["Ksub"] = 0
    
    dic["Ksub,Kgen"] = 0
    dic["Ksub,Kakk"] = 0
    dic["Ksub,Kdat"] = 0
    
    dic["Ksub,Kgen,Kakk"] = 0
    dic["Ksub,Kgen,Kdat"] = 0
    dic["Ksub,Kakk,Kdat"] = 0
    
    dic["Ksub,Kgen,Kakk,Kdat"] = 0
    
    for id in satzIDs:
        nominativ = False
        genitiv = False
        akkusativ = False
        dativ = False
        data = getSentenceData(id)
        for element in data:
            temp = data[element] # gibt die Fälle / Infos zu diesem Wort innerhalb des Satzes zurück (z.B. Wort "Am" mit Dativ)
            # wir betrachten subjekt, akkusativ und dativ
            if temp != None:
                if "Nom" in temp:
                    nominativ = True
                if "Gen" in temp:
                    genitiv = True
                if "Acc" in temp:
                    akkusativ = True
                if "Dat" in temp:
                    dativ = True
        # finde für jeden Satz den SBP heraus mit den gefundenen Informationen
        SBP = findSBP(nominativ, genitiv, akkusativ, dativ)
        # erhöhe dann entsprechenden Zähler
        dic[SBP] += 1
    return dic

# alle Frequenzen berechnen und als Vektor speichern. Gibt Liste mit allen Vektoren zurück
def calculateFreqs(allIDs):
    vectors = []
    dic = {}
    for verb in allIDs:
        freq = countFrequency(allIDs[verb])
        list = []
        for SBP in freq:
            list.append(freq[SBP])
        v = np.array([list[0], list[1], list[2], list[3], list[4], list[5], list[6], list[7], list[8]])
        vectors.append(v)
        dic[verb] = v
    return dic

# ---------------------------------------------------------------------------------------
# Clustering mit Density-Based Spatial Clustering of Applications with Noise, kurz DBSCAN
# ---------------------------------------------------------------------------------------
def getEpsilon(): # fragt Nutzer nach gewünschtem Wert für Epsilon und Mindestanzahl an Nachbarn
    print("Bitte Fließkommazahl für gewünschten Wert für Epsilon eingeben:")
    s = input()
    return float(s)

def getMinNeighbours():
    print("Bitte natürliche Zahl für gewünschtes Minimum an Nachbarn eingeben:")
    s = input()
    return int(s)

# bekommt einen Vektor vector und gibt eine Liste mit allen Nachbarn innerhalb von Epsilon zurück (inklusive vector selbst)
def regionQuery(vector, vectors, epsilon):
    allNeighbours = []
    for v in vectors:
        similarity = cosSim(v, vector)
        if((1 - similarity) < epsilon): # the vector itself will be added too
            allNeighbours.append(v)
    return allNeighbours

# Nimmt einen Vektor und alle bisherigen Cluster entgegen. Gibt True zurück, falls vector bereits in einem Cluster vorhanden ist, False sonst
def isInAnyCluster(vector, allClusters): # returns true if this vector is already in a cluster
    result = False
    for cluster in allClusters:
        for v in cluster:
            if((v == vector).all()):
                result = True
    return result

# Prüft, ob ein Vektor vector bereits in einem spezifischen Cluster vorkommt. Falls ja, return True
def isInThisCluster(vector, cluster):
    result = False
    for v in cluster:
        if((vector == v).all()):
            result = True
    return result

# Nimmt zwei Listen von Vektoren und verschmilzt diese, d.h. es wird eine Liste mit allen Vektoren ohne Duplikate zurückgegeben
def join(list1, list2):
    for v2 in list2:
        alreadyIn = False
        for v1 in list1:
            if((v1 == v2).all()):
                alreadyIn = True
        if(not(alreadyIn)):
            list1.append(v2)
    return list1

# Hauptfunktion. Erstellt Cluster. Nimmt Vektoren, sowie Werte für Epsilon und Mindestanzahl an Nachbarn entgegen.
# Gibt Tupel (allClusters, noise) zurück, wobei allClusters eine Liste von allen Clustern ist und noise eine Liste aller Vektoren, die als "Rauschen" klassifiziert wurden.
# Die Menge noise ist KEIN Cluster!
def DBSCAN(vectors, epsilon, minNeighbours):
    noise = []
    allClusters = []
    for vector in vectors:
        neighbours = regionQuery(vector, vectors, epsilon) # get all neighbours of a vector, including itself
        if(len(neighbours) < minNeighbours):
            noise.append(vector) # if not enough neighbours, this vector is noise. It will not be in any cluster
        else: # we have enough neighbours to potentially build a cluster
            cluster = expandCluster(vector, neighbours, allClusters, vectors, epsilon, minNeighbours) # create the cluster
            if(len(cluster) > 0): # we don´t want empty clusters
                allClusters.append(cluster)
    return (allClusters, noise)

# Schaut sich alle Nachbarn (neighbours) eines Vektors (vector) an und wiederum die Nachbarn der Nachbarn, um ein Cluster zu erstellen (unter Berücksichtung von epsilon und minNeighbours)
def expandCluster(vector, neighbours, allClusters, vectors, epsilon, minNeighbours):
    cluster = []
    if(not(isInAnyCluster(vector, allClusters))):
        cluster.append(vector) # this core vector will build a cluster, even if it is alone, since it has enough neighbours
    for n in neighbours:
        neighbourNeighbours = regionQuery(n, vectors, epsilon) # get all neighbours of each neighbour
        if(len(neighbourNeighbours) >= minNeighbours): # if n has enough neighbours, its a core vector. We join our previous neighbours with the new neighbours
            neighbours = join(neighbours, neighbourNeighbours)
        if(not(isInAnyCluster(n, allClusters)) and not(isInThisCluster(n, cluster))): # if the neighbour isn´t in any cluster yet, we add it to this cluster we are currently working on
            cluster.append(n)
    return cluster

# Funktion, die für einen Satzbauplanvektor das dazugehörige Verb ausgibt
def findVerbWithVector(vector, dic):
    keys = dic.keys()
    for key in keys:
        if((dic[key] == vector).all()):
            return key
    print("Kein Verb für diesen Vektor gefunden.")