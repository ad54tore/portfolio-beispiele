import os
import sys
import csv
import getPartsFromPDF as gp
import mapping as mp

from extracting_triples import Neowriter,Node
#######################
# VARIABLES
path = sys.argv[1] # path to the data set, for example C:/.../Datensatz
category = sys.argv[2] 


#######################
#FUNCTIONS

def comparisonToTXT(path, category, PDF, textLines):
    file1 = open(path + "/" + category + "/Comparisons/" + PDF + "comparisons.txt","w", encoding="utf8")
    file1.truncate(0) # wipe exisiting text 
    file1.writelines(textLines) 
    file1.close() #to change file access modes 
    print('Comparisons saved as ' + path + "/" + category + "/Comparisons/"+PDF+"comparisons.txt")

def resultsToTXT(path, category, results):
    file1 = open(path + "/" + category + "/results.txt","w", encoding="utf8")
    file1.truncate(0) # wipe exisiting text 
    file1.writelines(results) 
    file1.close() #to change file access modes 
    print('Results saved as ' + path + "/" + category + "/results.txt")


def getAllPDFNames(path, category):
    '''
    This function will get all the PDF names from a single category by reading the combined_csv_parts.
    
    :param category:
    Has to be a String. Example: 'ac_dc_power_modules'
    
    :return:
    Returns a list of all PDFNames
    '''
    PDFNames = []
    path = path + '/' + category + '/combined_csv_parts.csv'
    with open(path, 'r', encoding='utf-8-sig') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        iter = 0
        for row in csv_reader:  # go through each row of csv file
            if(iter == 0): #skip header
                iter += 1
            else:
                if(not row[0] == ''):
                    PDFNames.append(row[0])
    
    #remove duplicates
    PDFNames = list(dict.fromkeys(PDFNames))
    return PDFNames

def createGraph(path, category, rdata):
    cmd = 'python C:/Users/Arnold/PycharmProjects/Bachelor/Bachelor/extracting_triples.py  --insertonto --tables ' + path + '/' + category + '/RData/' + rdata + '.RData'
    os.system(cmd)
    print('Graph Database created!')

def deleteGraph():
    cmd = 'python C:/Users/Arnold/PycharmProjects/Bachelor/Bachelor/extracting_triples.py --reset'
    os.system(cmd)
    print('Graph Database deleted!')

def extractDataFromGraph():
    '''
    Assuming that the graph was already created and is online, this function will extract all triples from it.
    This happens with a query, which creates a csv file with all values in it. We then extract the triples from this csv.
    Converting the triples to tuples and then returning these tuples
    Example: (title, unit, value) -> (title, value + unit)
    
    :return:
    returns a list of tuples. Tuples look like this (title, value + unit). Example for tuple: ('Output Voltage', '200 Volt') 
    '''
    # Create values.csv, which contains all the information of the graph. 
    all = []
    cmd = 'python ' + 'C:/Users/Arnold/Downloads/queries.py --all'
    os.system(cmd) # will create values.csv with all data from a PDF in it
    string = 'C:/Users/Arnold/Downloads/values.csv'
    with open(string, encoding = "utf8") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        # time to find all triples of units, titles and numerics (for example 'Output Voltage = 200 Volt')
        lineCount = 0
        for row in csv_reader:
            if lineCount == 0:
                lineCount += 1
                #do nothing
            else:
                # row[4] is unit_short, row[5] is unit
                quadrupel = (row[1], row[4], row[6], row[2]) # (title, unit, value, quantity), for example ('Output Voltage', 'Volt', 200, "voltage")
                all.append(quadrupel)
    
    '''
    # Converting triples to tuples
    allTuples = []
    for triple in allTriples:
        s = triple[2] + ' ' + triple[1]
        tuple = (triple[0], s)
        allTuples.append(tuple)
    '''
    return all


def compareExact(PDF):
    '''
    This function takes a PDF and finds all parts, that have this PDF as their datasheet.
    As the name suggests, we are looking for exact matches. That means, that even if the correct attribute, for example ('Output Power I', '20 W'), is really
    close to the correct one, for example ('Output Power', '44 W'), it will not be a match.
    
    This function counts the number of all exact matches and returns it. This can be used to calculate further measures like F1-Measure or precision and recall
    
    :param PDF:
    Name of the PDF file WITH .pdf at the end
    
    :return:
    Returns the number of exact matches and failed matches in form of a tuple (exactMatches, noMatches). 
    '''
    PDF = PDF[:-4] # remove .pdf at the end
    uncertainPart = extractDataFromGraph() # the parts from the graph, which need to be controlled. Is a list of tuples
    correctParts = gp.getPartsFromPDF(PDF) # the parts of which we know that they are correct. Is a list of lists. Each list consists of tuples
    
    #Testing
    #testTuple = ('Output Power', '30 W')
    #uncertainPart.append(testTuple)
    #Testing END
    
    #print(uncertainPart)
    #print(correctParts)
    
    noMatchCount = 0
    exactMatchCount = 0
    iter = 1
    for list in correctParts: # go through all the correct parts that were found with this PDF
        iter += 1
        for uTuple in uncertainPart: # go through all tuples
            for cTuple in list: # counter check them with the correct tuples and see if there are (exact) matches
                if(uTuple[0] == cTuple[0] and uTuple[1] == cTuple[1]):
                    print('Exact Match!'); print(uTuple, cTuple)
                    exactMatchCount += 1
                else:
                    noMatchCount += 1
    return (exactMatchCount, noMatchCount)

import numpy as np

def levenshtein(seq1, seq2):
    size_x = len(seq1) + 1
    size_y = len(seq2) + 1
    matrix = np.zeros ((size_x, size_y))
    for x in range(size_x):
        matrix [x, 0] = x
    for y in range(size_y):
        matrix [0, y] = y

    for x in range(1, size_x):
        for y in range(1, size_y):
            if seq1[x-1] == seq2[y-1]:
                matrix [x,y] = min(
                    matrix[x-1, y] + 1,
                    matrix[x-1, y-1],
                    matrix[x, y-1] + 1
                )
            else:
                matrix [x,y] = min(
                    matrix[x-1,y] + 1,
                    matrix[x-1,y-1] + 1,
                    matrix[x,y-1] + 1
                )
    #print (matrix)
    return (matrix[size_x - 1, size_y - 1])

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def compareLevenshtein(PDF, limit):
    '''
    This function takes a PDF and finds all parts, that have this PDF as their datasheet.
    As the name suggests, we are looking for exact matches. That means, that even if the correct attribute, for example ('Output Power I', '20 W'), is really
    close to the correct one, for example ('Output Power', '44 W'), it will not be a match.
    
    This function counts the number of all exact matches and returns it. This can be used to calculate further measures like F1-Measure or precision and recall
    
    :param limit:
    Wished Levenshtein Distance limit. Difference of two strings is not allowed to be higher than this limit
    Example: 'Berg' and 'Zwerg'. If limit = 1, then no match, because Levenshtein distance = 2. If limit >= 2, then match
    
    :param PDF:
    Name of the PDF file WITH .pdf at the end
    
    :return:
    Returns the number of exact matches and failed matches in form of a tuple (exactMatches, noMatches). 
    '''
    PDF = PDF[:-4] # remove .pdf at the end
    uncertainPart = extractDataFromGraph() # the parts from the graph, which need to be controlled. Is a list of tuples
    correctParts = gp.getPartsFromPDF(PDF) # the parts of which we know that they are correct. Is a list of lists. Each list consists of tuples
    
    #Testing
    #testTuple = ('Output Power', '30 W')
    #uncertainPart.append(testTuple)
    #Testing END
    
    #print(uncertainPart)
    #print(correctParts)
    
    noMatchCount = 0
    exactMatchCount = 0
    iter = 1
    for list in correctParts: # go through all the correct parts that were found with this PDF
        iter += 1
        for uTuple in uncertainPart: # go through all tuples
            for cTuple in list: # counter check them with the correct tuples and see if there are (exact) matches
                uString = uTuple[0] + uTuple[1] # combine tuple values to one string. Levenshtein on a long string has different results than levenshtein on two smaller strings
                cString = cTuple[0] + cTuple[1]
                if(hasNumbers(uString) and hasNumbers(cString)): # filter out those tuples, that do not contain any numbers / values. Very often ' W' or ' volt'
                    if(levenshtein(uString, cString) < limit):
                        print('Levenshtein Match!'); print(uTuple, cTuple)
                        exactMatchCount += 1
                    else:
                        noMatchCount += 1
    return (exactMatchCount, noMatchCount)

import matplotlib.pyplot as plt

def plotLevenshtein(input):
    '''
    Plots a graph with two functions. Red function displays the failed matches and green function the correct matches according to the different levenshtein distances.
    
    :param input:
    input is a list of sublists. Each sublist represents one PDF. These sublists contain n tuples, where n is the highest levenshtein distance used. 
    These tuples are the return value of the compareLevenshtein function, so a tuple (exactMatchCount, noMatchCount).
    
    :return:
    No return, just plot.
    '''
    howManyPDFs = len(input)
    
    
    matchCounts = []
    noMatchCounts = []
    n = len(input[0])
    for i in range(0, n): # create list with n zeros. n is how many different levenshtein distances we used, for example 1 to 12
        matchCounts.append(0)
        noMatchCounts.append(0)
    
    # count how many matches and no matches there where in total for each levenshtein distance
    for resultList in input: # go through the results for each PDF
        iter = 0
        for result in resultList: # count matches and no matches for each levenshtein distance. Do this for every PDF
            matchCounts[iter] += result[0]
            noMatchCounts[iter] += result[1]
            iter += 1
    
    x = []
    for i in range(1, n+1):
        x.append(i)
    y1 = matchCounts
    y2 = noMatchCounts
    
    #print(x)
    #print(y1)
    #print(y2)
    
    plt.plot(x, y1, 'g') # green function for correct matches
    plt.plot(x, y2, 'r') # red function for failed matches
    plt.xlabel('Levenshtein Distances')
    plt.ylabel('Number of Matches')
    plt.title('Levenshtein comparison for n = [0, ..., ' + str(n) + ']')
    plt.show()

def mapping(triple, category):
    value = triple[1]
    unit = triple[2]
    
    # possible differences for ac_dc_power_modules:
    # VDC / VAC instead of Volt
    # mA / kA instead of Ampere
    # mW / kW instead of Watt
    
def extractValue(string):
    '''
    :param string:
    Example: string = "- 40 C" or string "12 A"
    '''
    list = []
    count = string.count(" ")
    if("=" in string):
        s = "s"
    if(count == 1 or count == 2):
        list = string.split(" ")
    return list
    

def compareBoolean(path, category, PDF):
    '''
    Function will check the three attributes of triples. First, the numeric value. If this value is correct, then as second we check wether the unit is correct (f.e. 'Volt' vs 'volt').
    Lastly we check if the title is correct, even though in this step we expect differences and will be much more tolerant. 
    
    :param PDF:
    Name of the PDF file WITH .pdf at the end
    
    :return:
    Returns the number of exact matches and failed matches in form of a tuple (exactMatches, noMatches). 
    '''
    PDF = PDF[:-4] # remove .pdf at the end
    uncertainPart = extractDataFromGraph() # the parts from the graph, which need to be controlled. Is a list of triples
    correctParts = gp.getPartsFromPDF(path, category, PDF) # the parts of which we know that they are correct. Is a list of lists. Each list consists of tuples. Example: ("Output Voltage", "30 V")
    # Problem: Mouser data can look very different. Here are some examples: "30 V" (easy), "- 54 C" (easy), "33 deg" (easy), "233 to 245 V" (hard), "20 V at 12 A" (medium-hard)
    # We therefore decided to use the format "s int unit", where s can be "+" or "-", int is an integer or float, unit is the unit in short (mA instead of milliampere).
    # Reason: Most of the mouser cells have this format. 
    # In addition, we also have the quantity, for example "electric current" for values with ampere. We will use this quantity to determine a match / mismatch. 
    
    #Testing
    #testTuple = ('Output Power', '30 W')
    #uncertainPart.append(testTuple)
    #Testing END
    
    #print(uncertainPart)
    #print(correctParts)
    
    # statistical variables
    amountComparisons = 0
    totalTuples = 0
    truePositives = 0
    falsePositives = 0
    falseNegatives = 0
    
    invalidTuples = 0 # if our correct data has a quantity, but the values are too strange / difficult, invalidTuples goes up by 1
    
    # quantityCount stores, how many times each quantity exists (pipeline will most certainly have less counts for a quantity than the mouser data, so less precision and recall). 
    truePosQuantityCount = mp.get_quantities(category) # get all quantities for a category. We will count how many 'electric current', 'voltages'... we have in total
    falsePosQuantityCount = mp.get_quantities(category)
    falseNegQuantityCount = mp.get_quantities(category)
    
    # other stats
    noMatchCount = 0
    exactMatchCount = 0
    testResult = []
    textLines = []
    
    # Step 1: Count how many quantities we have in total. Example: electric current = 2344 times, voltage = 3445 times, ...
    for list in correctParts:
        #convert tuples to triples / attributes
        for cTuple in list:
            cTitle = cTuple[0]
            cCell = cTuple[1]
            cQuantity = mp.cameras_mapping(cTuple[0]) # "Forward Voltage" will become just "voltage"
            if(cQuantity == "none"): # if a mouser tuple has no quantity, it can't be used and will be skipped
                truePosQuantityCount['none'] += 1
                falsePosQuantityCount['none'] += 1
                falseNegQuantityCount['none'] += 1
                continue
            
            # extract value and unit out of cTuple
            cAttributes = extractValue(cCell)
            cSign = "+"
            cValue = 0
            cUnit = ""
            #print(cAttributes)
            if(cQuantity == 'resolution'):
                p = 'there are no easter eggs here, go away'
                #do nothing, resolution cannot be converted. Example: '1920 x 1080'
            elif(len(cAttributes) == 3): # we have a + or -, a value and a unit
                cSign = cAttributes[0]
                cValue = cSign+str(cAttributes[1])
                cValue = float(cValue) # float cast from string "-4.3" to float -4.3 
                cUnit = cAttributes[2].upper()
            elif(len(cAttributes) == 2): # we have a value and a unit
                # cSign = "+", but not important for positive numbers
                cValue = float(cAttributes[0]) 
                cUnit = cAttributes[1].upper()
            else: # we have no valid tuple with easy values
                invalidTuples += 1
                continue # skip this tuple
            
            totalTuples += 1
            
            cQuadruple = (cTitle, cValue, cUnit, cQuantity)
            noFoundMatch = True
            
            # Step 2: check whether the mouser quantity (voltage) is the same as the pipeline quantity (?)
            for uTriple in uncertainPart:
                uTitle = uTriple[0]
                uValue = uTriple[2]
                if(uValue == ''):
                    continue
                uValue = float(uValue)
                uUnit = uTriple[1].upper()
                uQuantity = uTriple[3]
                
                # save all comparisons
                uQuadruple = (uTitle, uValue, uUnit, uQuantity)
                # save all comparisons in text form
                line = str(cQuadruple) + ' -> ' + str(uQuadruple) + '\n'
                textLines.append(line)
                #print(cTitle, cValue, cUnit)
                #print(uTitle, uValue, uUnit)
                
                # check if quantity is the same
                if(uQuantity == cQuantity):
                    # Step 3: check if the value is the same
                    if(uValue == cValue):
                        # Step 4: check if unit is the same
                        if(uUnit == cUnit):
                            #print(cQuadruple, uQuadruple)
                            truePositives += 1
                            truePosQuantityCount[cQuantity] += 1
                            noFoundMatch = False
                            break # after successful match, we need to skip to the next iteration
            # Step 5: If we could't find a match, then we have a falseNegative
            if(noFoundMatch):
                falseNegQuantityCount[cQuantity] += 1
                falseNegatives += 1
    '''
    old
    iter = 1
    for list in correctParts: # go through all the correct parts that were found with this PDF
        iter += 1
        #convert tuples to triples / three attributes
        for cTuple in list:
            cSplitted = cTuple[1].split(' ')
            if(len(cSplitted) < 2):
                continue
            cTitle = cTuple[0]
            cValue = cSplitted[0]
            cUnit = cSplitted[1].upper()
            
            cTriple = (cTitle, cValue, cUnit)
            for uTriple in uncertainPart:
                # switch value and unit
                uTitle = uTriple[0]
                uValue = uTriple[2]
                uUnit = uTriple[1].upper()
                
                uTriple = (uTitle, uValue, uUnit)
                # save all comparisons in text form
                line = str(cTriple) + ' -> ' + str(uTriple) + '\n'
                textLines.append(line)
                #print(cTitle, cValue, cUnit)
                #print(uTitle, uValue, uUnit)
                
                if(uValue == cValue and uUnit == cUnit):
                    print('Boolean Match:')
                    print(uTriple)
                    print(uTitle, uValue, uUnit)
                    testResult.append(uTriple)
                    testResult.append((cTitle, cValue, cUnit))
                    exactMatchCount += 1
                else:
                    noMatchCount += 1
    '''
    # false positives are those uTuples, for which no match could be found. Number of lines in values.csv minus number of true positives
    for part in uncertainPart:
        value = part[2]
        quantity = part[3]
        if(not value == ''):
            keys = falsePosQuantityCount.keys()
            if(not quantity in keys):
                falsePosQuantityCount[quantity] = 0
            falsePosQuantityCount[quantity] += 1
    for key in falsePosQuantityCount:
        keys = truePosQuantityCount.keys()
        if(not key in keys):
            continue # we dont have 'power' in the truePosQuantityCount for example. It only occurs in our pipeline, so only in falsePositives, so skip it
        newValue = falsePosQuantityCount[key] - truePosQuantityCount[key]
        newValue = abs(newValue)
        falsePosQuantityCount[key] = newValue
    
    comparisonToTXT(path, category, PDF, textLines)
    return (truePosQuantityCount, falsePosQuantityCount, falseNegQuantityCount)

def mergeDic(dic1, dic2): # merges two dictionaries. The values will be added with each other. example: dic1 = {'a' : 3, 'b' : 3} + dic2 = {'a' : 2} -> newDic = {'a' : 5, 'b' : 3}
    for key in dic2:
        if key in dic1:
            dic1[key] += dic2[key]
        else:
            dic1[key] = dic2[key]
    return dic1

def f1(truePositives, falsePositives, falseNegatives):
    '''
    Parameters should be dictionary (look at mp.get_quantities)
    
    will calculate precision, recall and f1-measures
    '''
    totalTruePositives = 0
    totalFalsePositives = 0
    totalFalseNegatives = 0
    
    truePositives.pop('none', None)
    falsePositives.pop('none', None)
    falseNegatives.pop('none', None)
    
    macro_precisions = []
    macro_recalls = []
    
    # for every single quantity
    for quantity in truePositives:
        precision = 0
        recall = 0
        f1 = 0
        
        totalTruePositives += truePositives[quantity]
        totalFalseNegatives += falseNegatives[quantity]
        
        t1 = (truePositives[quantity] + falsePositives[quantity])
        t2 = (truePositives[quantity] + falseNegatives[quantity])
        
        if(t1 > 0):
            precision = truePositives[quantity] / t1
        if(t2 > 0):
            recall = truePositives[quantity] / t2
        if(precision + recall > 0):
            f1 = precision * recall
            f1 /= (precision + recall)
            f1 *= 2
        
        macro_precisions.append(precision)
        macro_recalls.append(recall)
        
        print('Quantity: ' + quantity)
        print('Precision: ' + str(precision))
        print('Recall: ' + str(recall))
        print('F1-Score: ' + str(f1))
        print('-----------------')
    
    # falsePositives is an exception, because sometimes we find quantities that mouser doesn't know, so we have more keys in the dictionary
    for key in falsePositives:
        totalFalsePositives += falsePositives[key]
    
    # micro f1 in total
    micro_precision = totalTruePositives / (totalTruePositives + totalFalsePositives)
    micro_recall = totalTruePositives / (totalTruePositives + totalFalseNegatives)
    
    micro_f1 = micro_precision * micro_recall
    micro_f1 /= (micro_precision + micro_recall)
    micro_f1 *= 2
    
    #macro f1 in total
    macro_precision = 0
    macro_recall = 0
    divider = len(macro_precisions)
    for prec in macro_precisions:
        macro_precision += prec
    for rec in macro_recalls:
        macro_recall += rec
    
    macro_precision /= divider
    macro_recall /= divider
    
    macro_f1 = macro_precision * macro_recall
    macro_f1 /= (macro_precision + macro_recall)
    macro_f1 *= 2
    
    print('-Category in total-')
    print('Micro_precision: ' + str(micro_precision))
    print('Micro_recall: ' + str(micro_recall))
    print('Micro-average F1-Score: ' + str(micro_f1))
    print('                       ')
    print('Macro_precision: ' + str(macro_precision))
    print('Macro_recall: ' + str(macro_recall))
    print('Macro-average F1-Score: ' + str(macro_f1))

def pipeline(path, category):
    '''
    This function combines several other functions to establish following pipeline for EVERY PDF in a category:
    Get all PDF files -> Use tabbyPDF on a PDF -> Get pdf.xml -> Insert pdf.xml into empty graph database -> Use --all query for results -> compare results with actual attributes from combined.csv
    
    At the end we will have a number of matches and no matches.
    
    :param category:
    Requires name of the wanted category. Has to be a String. Example: category = 'ac_dc_power_modules'
    
    :return:
    No return
    '''
    PDFNames = getAllPDFNames(path, category)
    # with the next line of code, you can limit the number of PDF's you would like to check instead of all PDF's, which can take a lot of time
    #PDFNames = PDFNames[:5]
    print('Number of unique PDFs: ' + str(len(PDFNames)))
    
    # results / statistics
    result = []
    totalMatches = 0
    totalNoMatches = 0
    tabbyCrashes = 0
    foundTables = 0
    noContent = 0
    
    # new stats
    cDic = {}
    uDic = {}
    invalidTuples = 0
    falseNegatives = {}
    falsePositives = {}
    truePositives = {}
    
    for PDF in PDFNames:
        # execute tabbyPDF on a PDF to create pdf.xml
        path1 = path + '/' + category + '/PDF/'
        path2 = path + '/' + category + '/XML/'
        commandString = 'java -jar C:/Users/Arnold/Downloads/tabbypdf-0.2-jar-with-dependencies.jar -f ' + path1 + PDF + ' -xml ' + path2 + PDF + '.xml'
        os.system(commandString)
        
        # if tabbyPDF does not find any tables, it crashes. This means that there will be no xml file. We skip this PDF by using 'continue'
        # check if file exsists
        if (not os.path.isfile(path2 + PDF + '.xml')):
            tabbyCrashes += 1
            continue # skip this PDF / iteration
        else:
            foundTables += 1
        
        # read XML and convert it into .Rdata format
        rdata = PDF + '.xml'
        cmd = 'python C:/Users/Arnold/Downloads/readXML.py ' + rdata + ' ' + path + ' ' + category
        os.system(cmd)
        
        # insert all_tables.Rdata into empty graph database
        createGraph(path, category, rdata)
        
        
        # Use comparison function. This function extracts the data from the graph and compares them to the actual data from mouser
        triple = compareBoolean(path, category, PDF)
        truePositives = mergeDic(triple[0], truePositives)
        falsePositives = mergeDic(triple[1], falsePositives)
        falseNegatives = mergeDic(triple[2], falseNegatives)
        
        '''
        old
        tuple = compareBoolean(path, category, PDF)
        countMatches = 0
        countNoMatches = 0
        
        countMatches += tuple[0]
        totalMatches += tuple[0]
        countNoMatches += tuple[1]
        totalNoMatches += tuple[1]
        matches = tuple[2]
        finalString = "PDF: " + PDF + ". Matches: " + str(countMatches) + ". Failed Matches: " + str(countNoMatches) + ". Comparisons: " 
        for m in range(0, len(matches), 2):
            finalString += str(matches[m])
            finalString += ' -> '
            finalString += str(matches[m+1])
            finalString += '| '
        finalString += '\n'
        result.append(finalString)
        '''
        '''
        # Use different levenshtein distances to create a plot
        l = []
        for limit in range(1, 51):
            l.append(compareLevenshtein(PDF, limit))
        result.append(l)
        '''
        
        
        # delete graph for the next PDF iteration
        deleteGraph()
        
        
    #Final result
    print('Final result:')
    print(truePositives)
    print(falsePositives)
    print(falseNegatives)
    f1(truePositives, falsePositives, falseNegatives)
    resultsToTXT(path, category, result)
    
    #print('Matches in total: ' + str(totalMatches))
    #print('Failed Matches in total: ' + str(totalNoMatches))
    print('tabbyPDF found ' + str(foundTables) + ' tables in PDFs.')
    print('tabbyPDF crashed ' + str(tabbyCrashes) + ' times.')
    #print('Triples with an empty attribute:' + str(noContent))
    
    '''
    print('Plotting graph...')
    plotLevenshtein(result)
    '''



#######################
# MAIN / EXECUTION
#t = extractDataFromGraph()
#c = compare(filename)
#createGraph()
#deleteGraph()

#pipeline(path, category)
import csv
with open("C:/Users/Arnold/Downloads/Datensatz/cameras/combined_csv_parts.csv","r", newline='') as source:
    rdr= csv.reader( source )
    with open("C:/Users/Arnold/Downloads/Datensatz/cameras/bla.csv","w", newline='') as result:
        wtr= csv.writer( result )
        for r in rdr:
            wtr.writerow( (r[0], r[1], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[16], r[17], r[18], r[19]) )