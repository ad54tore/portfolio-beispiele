control.py ist sozusagen die 'Main' für meine Bachelorarbeit, an der ich zur Zeit arbeite.
Es werden Graphdatenbanken in Neo4j erstellt und zusammengefasst wird am Ende
ein F1-Measure ausgerechnet mit dem gegebenen Datensatz, der durch eine Pipeline geschickt wird.

clustering.py war ein Projekt, bei dem Satzbaupläne als Vektoren dargestellt werden sollten
und anschließend geclustert werden sollten. Mit DBSCAN-Clustering hat das relativ gut geklappt
und das Ergebnis waren 0-5 Cluster, wobei ähnliche Satzbaupläne dann im selben Cluster zu finden waren.